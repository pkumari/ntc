import piplates.DAQCplate as DAQC
import time, math, os, datetime
import RPi.GPIO as GPIO

# Define constants
B = 3892  # From data sheet of NTCs
R0 = 10000  # (Ohms)
sleep_time = 1  # (s)
steinhart = 0  # (degrees C)
#dew_point = 0
t0 = time.time()

now = datetime.datetime.now()
year = now.year
month = now.month
day = now.day
hour = now.hour
min = now.minute
sec = now.second


# Start main loop
#while True:
Vs = DAQC.getADC(0, 8)  # Supply voltage (V)
pot1 = DAQC.getADC(0, 0)  # This get the potential from channel 0 on the analog input (V)
#pot2 = DAQC.get        ADC(0, 1)
#pot3 = DAQC.getADC(0, 2)
#pot4 = DAQC.getADC(0, 3)
#pot5 = DAQC.getADC(0, 4)


pots = [pot1] #, pot2, pot3, pot4, pot5]
temps = []

for pot in pots:
        Rn = R0 * (Vs/pot - 1)  # Derive from KVL, (Ohms)
    # Steinhart equation
        steinhart = Rn / R0  # (R/Ro)
        steinhart = math.log(steinhart)  # ln(R/R0)
        steinhart /= B
        steinhart += 1.0 / (25 + 273.15)  # + (1/T0)
        steinhart = 1.0 / steinhart  # Invert
        steinhart -= 273.15  # Convert to C

        temp = steinhart
        temps.append(temp)
        
now = time.time()


print("%5.1f,%5.1f"  % (now-t0,temps[0] ))
